# Convertisseur de formats de fichiers fixes

## Installer

```shell
cargo install --path .
```

## Utilisation

```
Usage: fffc <input_file> <metadata_file> <output_file>

Arguments:
  <input_file>          the file you want to convert
  <metadata_file>       the metadata file describing the file structure
  <output_file>         the output file

Example:
fffc input.txt metadata.csv output.csv
```

## Exemple

`input.txt`

```text
1970-01-01John Smith 81.5
1975-01-31Jane Doe 61.1
1988-11-28Bob Big 102.4
```

`metadata.csv`

```text
Date de naissance,10,date
Prénom,15,chaîne
Nom de famille,15,chaîne
Poids,5,numérique
```

`fffc input.txt metadata.csv output.csv` donnera

`output.csv`

```text
Date de naissance,Prénom,Nom de famille,Poids
01/01/1970,John,Smith,81.5
31/01/1975,Jane,Doe,61.1
28/11/1988,Bob,Big,102.4
```

## Tests

Pour lancer les tests, il faut éxecuter `cargo test` 