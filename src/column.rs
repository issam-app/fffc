use std::str::FromStr;

use colored::Colorize;

use crate::detailed_error::DetailedError;

#[derive(Debug, PartialEq)]
pub enum ColumnType {
    STRING,
    NUMBER,
    DATE,
}

impl FromStr for ColumnType {
    type Err = DetailedError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "chaîne" => Ok(Self::STRING),
            "numérique" => Ok(Self::NUMBER),
            "date" => Ok(Self::DATE),
            t => Err(
                DetailedError::new(format!("unknown column type {}", t.underline()))
                    .hint("Valid type are 'chaîne', 'numérique' or 'date'".to_string())
                    .example("Prénom,15,chaîne".to_string())
            ),
        }
    }
}

#[derive(Debug)]
pub struct Column {
    pub name: String,
    pub max_size: usize,
    pub column_type: ColumnType,
}

impl FromStr for Column {
    type Err = DetailedError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(",").collect();

        if parts.len() != 3 {
            let error_message = match parts.len() {
                1 => format!("Missing field {}", "max_size".underline()),
                2 => format!("Missing field {}", "type".underline()),
                _ => format!("Invalid column metadata format"),
            };

            return Err(
                DetailedError::new(error_message)
                    .line(s.to_string())
                    .width(s.len())
                    .hint("The right format is: <name>,<max_size>,<type>".to_string())
                    .example("Prénom,15,chaîne".to_string())
            );
        }
        let name = parts[0].parse::<String>().unwrap();
        let max_size = parts[1].parse::<usize>()
            .map_err(|e| {
                DetailedError::new(e.to_string())
                    .width(parts[0].len() + 1)
                    .length(parts[1].len())
                    .line(s.to_string())
            })?;
        let column_type = parts[2].parse::<ColumnType>()
            .map_err(|e| {
                e.width(parts[0].len() + parts[1].len() + 1)
                    .length(parts[2].len())
                    .line(s.to_string())
            })?;

        return Ok(Column { name, max_size, column_type });
    }
}