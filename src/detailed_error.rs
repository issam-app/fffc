use std::fmt::{Display, Formatter};

use colored::Colorize;

#[derive(Default, Debug)]
pub struct DetailedError {
    pub message: String,
    pub filename: Option<String>,
    pub line: Option<String>,
    pub line_nb: Option<usize>,
    pub width: Option<usize>,
    pub length: Option<usize>,
    pub hint: Option<String>,
    pub example: Option<String>,
    pub field: Option<String>,
    pub kind: ErrorKind,
}

#[derive(Default, Debug, PartialEq)]
pub enum ErrorKind {
    INPUT,
    METADATA,
    OUTPUT,
    #[default]
    ELSE,
}

impl DetailedError {
    pub fn new(message: String) -> DetailedError {
        DetailedError {
            message,
            ..DetailedError::default()
        }
    }

    pub fn filename(mut self, filename: String) -> Self {
        self.filename = Some(filename);
        self
    }

    pub fn line(mut self, line: String) -> Self {
        self.line = Some(line);
        self
    }

    pub fn line_nb(mut self, line_nb: usize) -> Self {
        self.line_nb = Some(line_nb);
        self
    }

    pub fn width(mut self, width: usize) -> Self {
        self.width = Some(width);
        self
    }

    pub fn length(mut self, length: usize) -> Self {
        self.length = Some(length);
        self
    }

    pub fn hint(mut self, hint: String) -> Self {
        self.hint = Some(hint);
        self
    }

    pub fn example(mut self, example: String) -> Self {
        self.example = Some(example);
        self
    }

    pub fn kind(mut self, kind: ErrorKind) -> Self {
        self.kind = kind;
        self
    }

    pub fn field(mut self, field: String) -> Self {
        self.field = Some(field);
        self
    }
}

impl Display for DetailedError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{} {}", "Error:".bold().red(), self.message.red())?;
        if let Some(filename) = &self.filename {
            write!(f, "{} {}", "Reading".green(), filename.green().bold())?;
            if let Some(line_nb) = self.line_nb {
                write!(f, "{}{}", ":".green(), line_nb.to_string().green())?;
                if let Some(width) = self.width {
                    write!(f, "{}{}", ":".green(), width.to_string().green())?;
                }
            }
            write!(f, "\n")?;
        }
        if let Some(field) = &self.field {
            writeln!(f, "{} {}", "Proccessing field".bright_green(), field.bold().bright_green())?;
        }
        if let Some(l) = &self.line {
            writeln!(f, "--> {}", l)?;
            if let Some(w) = self.width {
                writeln!(f, "{:>w$}{:^>l$}", "^", "", w = w + 5, l = self.length.unwrap_or(1) - 1)?;
            }
        }
        if let Some(hint) = &self.hint {
            writeln!(f, "{} {}", "Hint:".bold().yellow(), hint.yellow())?;
        }
        if let Some(example) = &self.example {
            writeln!(f, "{} {}", "Example:".bold().blue(), example.blue())?;
        }
        Ok(())
    }
}