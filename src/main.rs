use std::{env, process};
use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, Read, Write};

use chrono::NaiveDate;
use colored::Colorize;

use crate::column::{Column, ColumnType};
use crate::detailed_error::{DetailedError, ErrorKind};
use crate::detailed_error::ErrorKind::{INPUT, METADATA, OUTPUT};

mod column;
mod detailed_error;

#[cfg(test)]
mod tests;

const BINARY_NAME: &str = env!("CARGO_PKG_NAME");

fn show_usage() {
    println!(
        "{} {} <input_file> <metadata_file> <output_file>\n",
        "Usage:".underline().bold(),
        BINARY_NAME.bold()
    );

    println!("{}", "Arguments:".bold().underline());
    println!("  <input_file>\t\tthe file you want to convert");
    println!("  <metadata_file>\tthe metadata file describing the file structure");
    println!("  <output_file>\t\tthe output file\n");

    println!("{}", "Example:".bold().underline());
    println!("{} input.txt metadata.csv output.csv", BINARY_NAME.bold());
}

fn open_file(filename: &str) -> Result<File, DetailedError> {
    File::open(filename).map_err(|e| {
        DetailedError::new(e.to_string())
            .filename(filename.to_string())
            .hint("Make sure file exists with correct permissions".to_string())
    })
}

fn read_columns_metadata(metadata: &mut dyn Read) -> Result<Vec<Column>, DetailedError> {
    let reader = BufReader::new(metadata);
    let mut columns = vec![];

    for (i, line) in reader.lines().enumerate() {
        let line = line.map_err(|e| {
            DetailedError::new(e.to_string())
                .line_nb(i).kind(METADATA)
        })?;
        if line.is_empty() {
            continue;
        }
        columns.push(line.parse::<Column>().map_err(|e| {
            e.line_nb(i).kind(METADATA)
        })?);
    }

    return Ok(columns);
}

fn parse_field(field: &str, column: &Column) -> Result<String, DetailedError> {
    match column.column_type {
        ColumnType::STRING => {
            Ok(field.to_string())
        }
        ColumnType::NUMBER => {
            match field.parse::<f32>() {
                Ok(f) => Ok(f.to_string()),
                Err(e) => return Err(
                    DetailedError::new(e.to_string())
                        .hint("Use . as decimal separator".to_string())
                        .example("16.2".to_string())
                ),
            }
        }
        ColumnType::DATE => {
            match NaiveDate::parse_from_str(field, "%Y-%m-%d") {
                Ok(date) => Ok(date.format("%d/%m/%Y").to_string()),
                Err(e) => {
                    return Err(
                        DetailedError::new(e.to_string())
                            .hint("Date format is %Y-%m-%d".to_string())
                            .example("1999-10-19".to_string())
                    );
                }
            }
        }
    }
}

fn extract_line(line: String, columns_metadata: &Vec<Column>, record: &mut csv::StringRecord)
                -> Result<(), DetailedError> {
    let mut chars = line.chars();
    let mut idx = 0;

    for column in columns_metadata {
        let mut field = String::new();
        let mut i = 0;
        let mut prematured_exit = false;

        while let Some(n) = chars.next() {
            i += 1;
            idx += 1;
            if n.is_whitespace() && !field.is_empty() {
                prematured_exit = true;
                break;
            }
            field.push(n);
            if i == column.max_size {
                break;
            }
        }
        if field.is_empty() {
            let line_copy = line.clone();
            return Err(
                DetailedError::new(format!("Field {} missing", column.name))
                    .width(idx)
                    .line(line_copy)
                    .kind(INPUT)
            );
        }
        let parsed_field = parse_field(&field, column).map_err(|e| {
            let offset = field.len() + if prematured_exit { 1 } else { 0 };
            let line_copy = line.clone();
            e.line(line_copy)
                .field(column.name.to_string())
                .width(if idx >= offset { idx - offset } else { 0 })
                .length(field.len())
                .kind(INPUT)
        })?;
        record.push_field(&parsed_field);
    }
    Ok(())
}

fn fixed_format_to_csv(input: &mut dyn Read, metadata: &mut dyn Read, output: &mut dyn Write)
                       -> Result<usize, DetailedError>
{
    let mut lines_written = 0;

    let columns_metadata = read_columns_metadata(metadata)?;
    let reader = BufReader::new(input);
    let mut out = csv::Writer::from_writer(output);

    let mut record = csv::StringRecord::new();

    columns_metadata.iter().for_each(|col| {
        record.push_field(col.name.as_str());
    });
    out.write_record(&record).map_err(|e| {
        DetailedError::new(e.to_string())
            .kind(OUTPUT)
    })?;
    record.clear();
    lines_written += 1;

    for (line_nb, line) in reader.lines().enumerate() {
        let line = line.map_err(|e| {
            DetailedError::new(e.to_string())
                .line_nb(line_nb)
        })?;
        if line.is_empty() {
            continue;
        }
        extract_line(line.clone(), &columns_metadata, &mut record).map_err(|e| {
            e.line_nb(line_nb).kind(INPUT)
        })?;
        out.write_record(&record).map_err(|e| {
            DetailedError::new(e.to_string())
                .kind(OUTPUT)
        })?;
        record.clear();
        lines_written += 1;
    }

    return Ok(lines_written);
}

fn fffc(input_filename: &str, metadata_filename: &str, output_filename: &str)
        -> Result<usize, DetailedError> {
    let mut metadata_file = open_file(metadata_filename)?;
    let mut input_file = open_file(input_filename)?;
    let mut output_file = OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open(output_filename)
        .map_err(|e| {
            DetailedError::new(e.to_string())
                .filename(output_filename.to_string())
        })?;

    return fixed_format_to_csv(
        &mut input_file,
        &mut metadata_file,
        &mut output_file,
    ).map_err(|e| {
        match e.kind {
            INPUT => e.filename(input_filename.to_string()),
            METADATA => e.filename(metadata_filename.to_string()),
            OUTPUT => e.filename(output_filename.to_string()),
            ErrorKind::ELSE => e,
        }
    });
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 4 {
        show_usage()
    } else {
        match fffc(&args[1], &args[2], &args[3]) {
            Err(e) => {
                eprintln!("{}", e);
                process::exit(1);
            }
            Ok(nb) => {
                println!("Successfully written {} {} into {}!",
                         nb,
                         if nb > 1 { "lines" } else { "line" },
                         &args[3]
                )
            }
        }
    }
}
