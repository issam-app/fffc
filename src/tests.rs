use std::io::Cursor;

use crate::{extract_line, read_columns_metadata};
use crate::column::{Column, ColumnType};
use crate::detailed_error::ErrorKind;
use crate::detailed_error::ErrorKind::INPUT;

#[test]
fn metadata_too_much_field() {
    let col = "Date de naissance,10,date,...";
    let mut r = Cursor::new(col);

    let result = read_columns_metadata(&mut r);

    if let Err(e) = result {
        assert_eq!(e.kind, ErrorKind::METADATA, "Error Kind should be Metadata");
    } else {
        assert!(false, "Should be an error");
    }
}

#[test]
fn metadata_missing_field() {
    let col = "Date de naissance,10";
    let mut r = Cursor::new(col);

    let result = read_columns_metadata(&mut r);

    if let Err(e) = result {
        assert_eq!(e.kind, ErrorKind::METADATA, "Error Kind should be Metadata");
    } else {
        assert!(false, "Should be an error");
    }
}

#[test]
fn invalid_max_size() {
    let col = "Date de naissance,abc,data";
    let mut r = Cursor::new(col);

    let result = read_columns_metadata(&mut r);

    if let Err(e) = result {
        assert_eq!(e.kind, ErrorKind::METADATA, "Error Kind should be Metadata");
    } else {
        assert!(false, "Should be an error");
    }
}

#[test]
fn invalid_column_type() {
    let col = "Date de naissance,10,invalid";
    let mut r = Cursor::new(col);

    let result = read_columns_metadata(&mut r);

    if let Err(e) = result {
        assert_eq!(e.kind, ErrorKind::METADATA, "Error Kind should be Metadata");
    } else {
        assert!(false, "Should be an error");
    }
}

#[test]
fn valid_column() {
    let col = "Date de naissance,10,date";

    let result = col.parse::<Column>();

    if let Ok(col) = result {
        assert_eq!(col.name, "Date de naissance");
        assert_eq!(col.max_size, 10);
        assert_eq!(col.column_type, ColumnType::DATE);
    } else {
        assert!(false, "Should result a single Column");
    }
}

#[test]
fn line_to_csv() {
    let metadata_str = "Date de naissance,10,date\nAge,5,numérique\nPrénom,5,chaîne";
    let mut r = Cursor::new(metadata_str);

    let metadata = read_columns_metadata(&mut r);

    assert!(metadata.is_ok());

    let metadata = metadata.unwrap();
    let line = "1999-10-1923 Issam".to_string();
    let mut record = csv::StringRecord::new();

    let result = extract_line(line, &metadata, &mut record);

    assert!(result.is_ok());

    assert_eq!(record.get(0), Some("19/10/1999"));
    assert_eq!(record.get(1), Some("23"));
    assert_eq!(record.get(2), Some("Issam"));
}

#[test]
fn invalid_date_type() {
    let metadata_str = "Date de naissance,10,date";
    let mut r = Cursor::new(metadata_str);

    let metadata = read_columns_metadata(&mut r);

    assert!(metadata.is_ok());

    let metadata = metadata.unwrap();
    let line = "1999-19-10".to_string();
    let mut record = csv::StringRecord::new();

    let result = extract_line(line, &metadata, &mut record);

    if let Err(e) = result {
        assert_eq!(e.kind, INPUT);
    } else {
        assert!(false, "Should be an error");
    }
}

#[test]
fn invalid_number_type() {
    let metadata_str = "Age,10,numérique";
    let mut r = Cursor::new(metadata_str);

    let metadata = read_columns_metadata(&mut r);

    assert!(metadata.is_ok());

    let metadata = metadata.unwrap();
    let line = "abc".to_string();
    let mut record = csv::StringRecord::new();

    let result = extract_line(line, &metadata, &mut record);

    if let Err(e) = result {
        assert_eq!(e.kind, INPUT);
    } else {
        assert!(false, "Should be an error");
    }
}

#[test]
fn empty_string() {
    let metadata_str = "Prénom,10,chaîne";
    let mut r = Cursor::new(metadata_str);

    let metadata = read_columns_metadata(&mut r);

    assert!(metadata.is_ok());

    let metadata = metadata.unwrap();
    let line = "".to_string();
    let mut record = csv::StringRecord::new();

    let result = extract_line(line, &metadata, &mut record);

    if let Err(e) = result {
        assert_eq!(e.kind, INPUT);
    } else {
        assert!(false, "Should be an error");
    }
}